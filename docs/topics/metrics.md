# Metrics 

Datadog, Opstrace, etc. which provide an entire stack can be found in the [Platforms](../platforms) topic. 

## Tools

### Prometheus 

- [Website](https://prometheus.io/)
- [Documentation](https://prometheus.io/docs/introduction/overview/)

#### Facts

- Started in 2012
- [Open Source](https://github.com/prometheus)
- [CNCF graduated project in 2018](https://www.cncf.io/announcements/2018/08/09/prometheus-graduates/)

#### Hot Topics 

- [PromQL](https://prometheus.io/docs/prometheus/latest/querying/basics/) query language.
- [Metric Types](https://prometheus.io/docs/concepts/metric_types/)
- [Exporters](https://prometheus.io/docs/instrumenting/exporters/) where Prometheus can scrape metrics from.
- [Instrumentation](https://prometheus.io/docs/instrumenting/clientlibs/) for your app source, exposing `/metrics`. 



