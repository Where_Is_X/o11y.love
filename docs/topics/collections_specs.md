# Collections/Specs

All collections and specifications which distribute also tools and methods. 

## Overview

### eBPF

> Instead of relying on static counters and gauges exposed by the operating system, eBPF enables the collection & in-kernel aggregation of custom metrics and generation of visibility events based on a wide range of possible sources. 

- [Website](https://ebpf.io/)

<!-- TODO: add more resources -->

### OpenTelemetry

> OpenTelemetry is a set of APIs, SDKs, tooling and integrations that are designed for the creation and management of telemetry data such as traces, metrics, and logs. 

- [Website](https://opentelemetry.io/)
- [Documentation](https://opentelemetry.io/docs/)

#### Facts

- Bring your own backend. OpenTelemetry provides the specification and collector only. 
    - KubeCon NA 2021: [OpenTelemetry Collector Deployment Patterns](https://www.youtube.com/watch?v=WhRrwSHDBFs) 
- Support for traces, metrics, logs. 

#### Hot Topics

- [CI/CD Observability: Tracing with OpenTelemetry](https://gitlab.com/gitlab-org/gitlab/-/issues/338943) (GitLab proposal)

### OpenSLO

> OpenSLO is a service level objective (SLO) language that declaratively defines reliability and performance targets using a simple YAML specification.

The project provides specification, tools, and more tools. 

- [Website](https://openslo.com/)

#### Facts

- A specification makes it easier to integrate and deploy common tools. 

#### Hot Topics


