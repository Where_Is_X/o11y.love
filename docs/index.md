# o11y.love - learn Observability

Getting started with Observability, or looking for new ideas for your own environments? There are so many great resources out there - you've come to a place trying to collect them all :)

## Introduction

o11y.love is a collection of helpful URLs and tutorials, originally started by Michael Friedrich, [@dnsmichi](https://twitter.com/dnsmichi) in January 2022. Created for and maintained by the community. 

`o11y` is a numeronym similar to `k8s`. `k8s = k <8 chars> s = Kubernetes`. `o11y = o <11 chars> y = Observability`.

> Please note that we endorse free learning resources and try to keep o11y.love informal only, no advertisements or product preferences. Tools and names are sorted in alpha numeric order. Missing a tool or want to provide feedback? See [Contributing](contributing.md). 

o11y.love is inspired by amazing community resources:

- [o11y.news](https://o11y.news) created by [Michael Hausenblas](https://twitter.com/mhausenblas) 
- [100daysofkubernetes.io](https://100daysofkubernetes.io) created by [Anaïs Urlichs](https://twitter.com/urlichsanais)

## Topics

- [Metrics](topics/metrics.md)
- [Tracing](topics/tracing.md)
- [Logs/Events](topics/logs_events.md)
- [Profiling](topics/profiling.md)

and much more ...

- [Collections/Specs](topics/collections_specs.md)
- [SLO](topics/slo.md)
- [Platforms](topics/platforms.md)


## Use Cases

[Use cases](use_cases.md) provide more Observability scenarios, best practices, and more. They are waiting for [your stories](contributing.md) :)

## Learning Resources

[Learning Resources](learning_resources.md) include newsletters, meetups, trainings & workshops and more. 

## Contributing

Add your knowledge and follow our [Contributing](contributing.md) guide. 

## License

[![License: CC BY-NC-SA 4.0](https://img.shields.io/badge/License-CC_BY--NC--SA_4.0-lightgrey.svg)](https://creativecommons.org/licenses/by-nc-sa/4.0/)
